<?php

namespace App\DataFixtures;

use App\Entity\Action;
use Doctrine\Persistence\ObjectManager;

class ActionFixture extends AppFixtures
{
    const ACTION_ASSIGN_TASKS = 'assign tasks';
    const ACTION_DRAW = 'draw';
    const ACTION_COMMUNICATE_WITH_MANAGER = 'communicate with manager';
    const ACTION_TEST_CODE = 'test code';
    const ACTION_WRITE_CODE = 'write code';

    private function getActionNames()
    {
        return [
            self::ACTION_ASSIGN_TASKS,
            self::ACTION_COMMUNICATE_WITH_MANAGER,
            self::ACTION_DRAW,
            self::ACTION_TEST_CODE,
            self::ACTION_WRITE_CODE,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $references = [];

        foreach ($this->getActionNames() as $actionName) {
            $action = new Action();
            $action->setName($actionName);
            $references[$actionName] = $action;
            $manager->persist($action);
        }

        $manager->flush();

        foreach ($references as $name => $action) {
            $this->addReference($name, $action);
        }
    }
}
