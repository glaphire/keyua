<?php

namespace App\DataFixtures;

use App\Entity\Employee;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class EmployeeFixture extends AppFixtures implements DependentFixtureInterface
{
    const EMPLOYEE_DESIGNER = 'Designer';
    const EMPLOYEE_DEVELOPER = 'Developer';
    const EMPLOYEE_MANAGER = 'Manager';
    const EMPLOYEE_QA_ENGINEER = 'QA Engineer';

    private function getEmployeesToActionsMap()
    {
        return
        [
            self::EMPLOYEE_DESIGNER => [
                ActionFixture::ACTION_DRAW,
                ActionFixture::ACTION_COMMUNICATE_WITH_MANAGER
            ],
            self::EMPLOYEE_DEVELOPER => [
                ActionFixture::ACTION_WRITE_CODE,
                ActionFixture::ACTION_TEST_CODE,
                ActionFixture::ACTION_COMMUNICATE_WITH_MANAGER,
            ],
            self::EMPLOYEE_MANAGER => [
                ActionFixture::ACTION_ASSIGN_TASKS,
            ],
            self::EMPLOYEE_QA_ENGINEER => [
                ActionFixture::ACTION_TEST_CODE,
                ActionFixture::ACTION_ASSIGN_TASKS,
                ActionFixture::ACTION_COMMUNICATE_WITH_MANAGER,
            ],
        ];
    }

    public function load(ObjectManager $manager)
    {
        foreach ($this->getEmployeesToActionsMap() as $employeeName => $actionNames) {
            $employee = new Employee();
            $employee->setName($employeeName);

            foreach ($actionNames as $actionName) {
                $action = $this->getReference($actionName);
                $employee->addAction($action);
            }

            $manager->persist($employee);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ActionFixture::class,
        ];
    }
}
