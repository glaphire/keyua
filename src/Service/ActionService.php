<?php

namespace App\Service;

use App\Entity\Action;
use App\Repository\ActionRepository;
use Exception;

class ActionService
{
    private ActionRepository $actionRepository;

    public function __construct(ActionRepository $actionRepository)
    {
        $this->actionRepository = $actionRepository;
    }

    public function getActionByName(string $name): Action
    {
        $action = $this->actionRepository->findOneBy([
            'name' => $name,
        ]);

        if (empty($action)) {
            throw new Exception(
                sprintf('Action with name "%s" not found', $name)
            );
        }

        return $action;
    }
}
