<?php

namespace App\Service;

use App\Entity\Action;
use App\Entity\Employee;
use App\Repository\EmployeeRepository;
use Exception;

class EmployeeService
{
    private EmployeeRepository $employeeRepository;

    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * @param Employee $employee
     * @return string[]
     */
    public function getEmployeeAllowedActionsList(Employee $employee): array
    {
        $actionNamesList = [];

        foreach ($employee->getActions() as $action) {
            $actionNamesList[] = $action->getName();
        }

        return $actionNamesList;
    }

    public function getEmployeeByName(string $name): Employee
    {
        $employee = $this->employeeRepository->findOneBy([
            'name' => $name,
        ]);

        if (empty($employee)) {
            throw new Exception(
                sprintf('Employee with name "%s" not found', $name)
            );
        }

        return $employee;
    }

    public function canEmployeeDoAction(Employee $employee, Action $action): bool
    {
        $isAllowedAction = $this
            ->employeeRepository
            ->isActionInAllowed($employee->getName(), $action->getName());

        return $isAllowedAction;
    }
}
