<?php

namespace App\Repository;

use App\Entity\Action;
use App\Entity\Employee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method null|Employee find($id, $lockMode = null, $lockVersion = null)
 * @method null|Employee findOneBy(array $criteria, array $orderBy = null)
 * @method Employee[]    findAll()
 * @method Employee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Employee::class);
    }

    public function isActionInAllowed(string $employeeName, string $actionName): bool
    {
        return (bool)$this->createQueryBuilder('e')
            ->select('COUNT(e.id)')
            ->leftJoin('e.actions', 'a')
            ->where('e.name=:employeeName')
            ->andWhere('a.name=:actionName')
            ->setParameter('employeeName', $employeeName)
            ->setParameter('actionName', $actionName)
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }
}
