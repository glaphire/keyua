<?php

namespace App\Command;

use App\Service\ActionService;
use App\Service\EmployeeService;
use Exception;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CheckCanEmployeeDoActionCommand extends AbstractCommand
{
    protected static $defaultName = 'company:can';

    private EmployeeService $employeeService;
    private ActionService $actionService;

    public function __construct(EmployeeService $employeeService, ActionService $actionService, string $name = null)
    {
        parent::__construct($name);
        $this->employeeService = $employeeService;
        $this->actionService = $actionService;
    }

    protected function configure()
    {
        $this
            ->setDescription('Check that employee can do provided action')
            ->addArgument('employee', InputArgument::REQUIRED, 'Employee name')
            ->addArgument('action', InputArgument::REQUIRED, 'Action name')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $employeeName = $input->getArgument('employee');
        $actionName = $input->getArgument('action');

        try {
            $employee = $this->employeeService->getEmployeeByName($employeeName);
            $action = $this->actionService->getActionByName($actionName);
            $canEmployeeDoAction = $this->employeeService->canEmployeeDoAction($employee, $action);

            //ternary operator to force readable output of boolean value (otherwise will be 1/0)
            $io->text($canEmployeeDoAction ? 'true' : 'false');
        } catch (Exception $e) {
            $io->error($e->getMessage());

            return self::EXIT_CODE_ERROR;
        }

        return self::EXIT_CODE_SUCCESS;
    }
}
