<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;

abstract class AbstractCommand extends Command
{
    const EXIT_CODE_SUCCESS = 0;
    const EXIT_CODE_ERROR = 1;
}
