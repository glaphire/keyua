<?php

namespace App\Command;

use App\Service\EmployeeService;
use Exception;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ListEmployeeActionsByNameCommand extends AbstractCommand
{
    protected static $defaultName = 'company:employee';

    private EmployeeService $employeeService;

    public function __construct(EmployeeService $employeeService, string $name = null)
    {
        parent::__construct($name);
        $this->employeeService = $employeeService;
    }

    protected function configure()
    {
        $this
            ->setDescription('List actions that employee is allowed to do')
            ->addArgument('employeeName', InputArgument::REQUIRED, 'Employee name')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $employeeName = $input->getArgument('employeeName');

        try {
            $employee = $this->employeeService->getEmployeeByName($employeeName);
            $AllowedActionsList = $this->employeeService->getEmployeeAllowedActionsList($employee);

            $io->listing($AllowedActionsList);
        } catch (Exception $e) {
            $io->error($e->getMessage());

            return self::EXIT_CODE_ERROR;
        }

        return self::EXIT_CODE_SUCCESS;
    }
}
