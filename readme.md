# KeyUA Test Assignment

Test Assignment description: [link](https://docs.google.com/document/d/17KdDUIK3d7NaMalq0CQYvOMjtusSLxozEPj92N_nATw/edit?usp=sharing)

Preparation steps:
Copy .env file to .env.local and fill required variables:

```
   MYSQL_DATABASE=<database name>
   MYSQL_ROOT_PASSWORD=<password> 
   HOST_USER=<gid>:<uid>
   COMPOSER_AUTH={\"github-oauth\":{\"github.com\":\"<your token here>\"}}
```

Run commands
    
```
    $ ./docker_composer_build_local_env.sh
    $ docker-compose up -d
    $ docker-compose exec php composer.phar install
    $ docker-compose exec php bin/console doctrine:migrations:migrate
    $ docker-compose exec php bin/console doctrine:fixtures:load
```

Now you can check commands about employees' actions:

```
    $ docker-compose exec php bin/console company:employee "Developer"
    $ docker-compose exec php bin/console company:employee "QA Engineer"
```
and

```
    $ docker-compose exec php bin/console company:can "Developer" "Draw"
    $ docker-compose exec php bin/console company:can "QA Engineer" "Communicate with manager"
```
    
Allowed Actions: assign tasks, draw, communicate with manager, test code, write code.
    
Allowed Employees: Designer, Developer, Manager, QA Engineer.